# note: this should never truly be refernced since we are using relative assets
http_path = "/skin/frontend/rwd/default/"
css_dir = "../css"
sass_dir = "../scss"
images_dir = "../images"
javascripts_dir = "../js"
relative_assets = true

output_style = :expanded
#environment = :production
environment = :development

#The preceding causes Compass to look for Sass files in
#skin/frontend/rwd/default/scss if they can't be found in skin/frontend/customTheme/default/scss.
add_import_path "../../../rwd/default/scss"